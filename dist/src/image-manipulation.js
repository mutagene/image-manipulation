var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as fs from 'fs';
import * as R from 'ramda';
import md5 from 'js-md5';
import { execAsync } from './utils';
import { assertFileExists } from './asserts';
export const WIDTH = 635;
export const HEIGHT = 880;
const tempFolder = (process.env.TMP || process.env.PWD + '/tmp') + '/';
const isImageLayer = (layer) => layer.source !== undefined;
const isTextLayer = (layer) => layer.text !== undefined;
const isDrawingLayer = (layer) => layer.shape !== undefined;
const isLayer = (layer) => isImageLayer(layer) || isTextLayer(layer) || isDrawingLayer(layer)
    && layer.align === undefined;
const isLayerPile = (layer) => !(isImageLayer(layer) || isTextLayer(layer) || isDrawingLayer(layer))
    && layer.align !== undefined;
/**
 * Extrait la commande du layer
 */
const getCommand = (layer) => {
    if (isImageLayer(layer)) {
        assertFileExists(layer.source);
        // config.w / config.h -> valeur en pixel,
        //    ! pour forcer le cassage de ratio, '>' (only shrink larger images. ?
        //    sinon vérifier qu'il n'y ait qu'un des deux et faire '${w}x' ou 'x${h}'
        const resize = 100 * (layer.xResize || 1); // TODO: gérer le resize via ratio propre, ?
        return `convert -background transparent -resize ${resize}% ${layer.source}`;
    }
    if (isTextLayer(layer)) {
        const width = layer.width || 0;
        const height = layer.height || 0;
        const center = layer.center || false;
        const color = layer.fontColor || 'black';
        const weight = layer.weight || 'normal';
        // si on spécifie une size, on est en text align justify automatiquement
        const size = `-size ${width === 0 ? '' : width}x${height === 0 ? '' : height}`;
        const align = center ? '-gravity center' : '';
        return `convert -background transparent ${size} ${align} -fill ${color} "pango:<span size=\\"${layer.fontSize * 1000}\\" font_desc=\\"${layer.font}\\" weight=\\"${weight}\\">${layer.text}</span>"`;
    }
    if (isDrawingLayer(layer)) {
        if (layer.shape === 'circle') {
            if (layer.r === undefined) {
                throw new Error('circle with no r value');
            }
            return `convert -size ${2 * layer.r + 1}x${2 * layer.r + 1} xc:transparent -fill "${layer.color}" -draw "circle ${layer.r},${layer.r} ${2 * layer.r},${layer.r}"`;
        }
        throw new Error(`Unhandled shape ${layer.shape} for DrawingLayer`);
    }
    throw new Error('layer is not a Layer');
};
/**
 * Calcul du hash d'un Layer ou LayerPile, unique, contenant toutes les infos du layer
 * On ne calcule plus le hash des images sources comme avant,
 *    donc 2 sources identiques mais avec url différent donnent 2 hash !==
 */
export const getHash = layer => {
    if (layer.hash !== undefined)
        return layer.hash;
    // TODO: issue #8
    // layer.xResize = undefined
    // layer.yResize = undefined
    // layer.rotate = undefined
    return isLayerPile(layer)
        ? md5(JSON.stringify(Object.assign(Object.assign({}, layer), { layers: layer.layers.map(sub => getHash(sub)) })))
        : md5(JSON.stringify(layer));
};
/**
 * True si le fichier ${hash}.png existe dans le dossier temporaire
 */
const fileHashExists = hash => fs.existsSync(`${tempFolder}/${hash}.png`);
/**
 * Sauvegarde le layer (ou ne fait rien s'il existe déjà), retourne le hash
 */
const saveLayer = (layer) => __awaiter(void 0, void 0, void 0, function* () {
    const hash = getHash(layer);
    // existe déjà -> rien à faire
    // TODO: pour imageLayer, on va regarder si l image existe, pas de hash.jpg
    if (fileHashExists(hash)) {
        return hash;
    }
    // pour un layer simple, on appelle sa commande
    if (isLayer(layer)) {
        yield execAsync(`${getCommand(layer)} ${tempFolder}/${hash}.png`);
        return hash;
    }
    // LayerPile, plus de taf
    if (isLayerPile(layer)) {
        // TODO: enregistrer une image intermédiaire qui est composée de ces layers, en calculant le stack s'il y a lieu
        // pour chaque layer on lance saveLayer de la même manière
        // TODO: functionnal
        const savedLayers = [];
        // TODO: pour l'instant marche juste en bottom
        const x = 0;
        let y = 0;
        for (const current of layer.layers) {
            const subhash = yield saveLayer(current); // peut ne rien faire s'il existe déjà
            // calcul de la taille de l'image obtenue
            const format = yield execAsync(`identify -format "%w,%h," ${`${tempFolder}${subhash}.png`}`);
            const [w, h] = format.split(',');
            savedLayers.push({ hash: subhash, x: x, y: y, width: parseInt(w), height: parseInt(h) });
            // TODO: position_refencial doit être défini dans un pile layer bottom par exemple ? ou à la main pour le moment ?
            // switch (layer.align) {
            //   case 'bottom':
            //     break;
            //   default:
            //     break;
            // }
            // x += parseInt(w) // TODO: calcul selon le stack du layer et la taille du précédent l saved
            y += parseInt(h);
        }
        let command = `convert -background transparent `;
        // fs.mkdirSync(tempFolder)
        const hashes = yield Promise.all(savedLayers);
        // page them on an empty picture
        // TODO: sortir
        const pageSavedLayers = (value) => {
            return `-page +${value.x}+${value.y} ${tempFolder + value.hash + '.png'} +page `;
        };
        hashes.map(value => command += pageSavedLayers(value));
        // TODO: faire un tableau de command au lieu de concat, puis concat à la fin afin d'être sur de pas oublier de foutre des espaces
        // write picture
        yield execAsync(command + ` -mosaic png:${tempFolder + hash + '.png'}`);
        // page le tout avec le stack en utilisant les tailles
        return hash;
    }
    // TODO:
    throw new Error('this is not a Layer or LayerPile');
});
// const OVERDRIVE = false // TODO: parametric
/**
 * parcours les Layer et construit un tableau de profondeur avec les layer
 * en supprimant les doublons (et en mettant dans la profondeur la plus élevée le cas échéant)
 */
export const completeData = (data, layer, deep = 0) => {
    if (!isLayerPile(layer)) {
        return addLayerToData(data, layer, deep);
    }
    data = addLayerToData(data, layer, deep);
    for (const l of layer.layers) {
        data = completeData(data, l, deep + 1);
    }
    return data;
};
export const addLayerToData = (data, layer, deep = 0) => {
    var _a, _b;
    if (layer.hash === undefined) {
        throw Error(`${layer} have no hash`);
    }
    // on regarde si le layer est déjà dans les data
    for (const [d, v] of (data || []).entries()) {
        for (const [key] of (v || []).entries()) {
            if (v[key].hash === layer.hash) {
                // si oui, on compare la deep
                if (deep > d) {
                    // plus profond ce que qui est déjà dans data :
                    // on ajoute en supprimant l'ancienne
                    data[d].splice(key, key);
                    data[deep] = (_a = data[deep]) !== null && _a !== void 0 ? _a : [];
                    data[deep].push(layer);
                }
                return data;
            }
        }
    }
    // si non on l'ajoute
    data[deep] = (_b = data[deep]) !== null && _b !== void 0 ? _b : [];
    data[deep].push(layer);
    return data;
};
// ça dégage: il suffit de map
// export const compilePictures: (h: number, w: number) => (pics: PictureToCompile[]) => Promise<string[]> = (h, w) => {return async pics => {
//   // calcul du hash de chaque Layer et LayerPile (données modifiées direct)
//   for (const picKey in pics) {
//     for (const layKey in pics[picKey].picture.layers) {
//       pics[picKey].picture.layers[layKey].l = computeLayerHash(pics[picKey].picture.layers[layKey].l)
//     }
//   }
//   // TODO: sortir ça propre et tester cf !!!!!
//   let dataToProcess: (Layer | LayerPile)[][] = []
//   for (const picKey in pics) {
//     for (const layer of pics[picKey].picture.layers) {
//       dataToProcess = completeData(dataToProcess, layer.l)
//     }
//   }
//   dataToProcess = dataToProcess.filter(x => x !== null && x !== [])
//   // on a tous les calques avec leur hash, rangés par unicité de hash dans un tableau de deep
//   // pour chacun, soit existe, soit on doit la faire
//   for (const key in dataToProcess) {
//     await makeIt(dataToProcess[key])
//   }
//   return Promise.all(pics.map(compilePicture(h, w)))
// }}
export const makeIt = (arr) => __awaiter(void 0, void 0, void 0, function* () {
    return Promise.all(arr.map(saveLayer));
});
// calcule le hash d'un layer et lui ajoute, de manière récursive si LayerPile
export const computeLayerHash = (l) => {
    if (isLayerPile(l)) {
        const hashedLayers = l.layers.map(computeLayerHash);
        return Object.assign(Object.assign({}, l), { layers: hashedLayers, hash: getHash(Object.assign(Object.assign({}, l), { layers: hashedLayers })) });
    }
    return Object.assign(Object.assign({}, l), { hash: getHash(l) });
};
export const compilePicture = (picture, path) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const h = picture.height;
    const w = picture.width;
    let command = `convert -size ${w}x${h} xc:#fff `;
    for (const layKey in picture.layers) {
        picture.layers[layKey].l = computeLayerHash(picture.layers[layKey].l);
    }
    let dataToProcess = [];
    for (const layer of picture.layers) {
        dataToProcess = completeData(dataToProcess, layer.l);
    }
    dataToProcess = dataToProcess.filter(x => x !== null);
    // on a tous les calques avec leur hash, rangés par unicité de hash dans un tableau de deep
    // pour chacun, soit existe, soit on doit la faire
    for (const key in dataToProcess) {
        yield makeIt(dataToProcess[key]);
    }
    // write all layer in temp with name = hash
    const promiseSave = picture.layers.map((value) => __awaiter(void 0, void 0, void 0, function* () {
        if (value.l.hash === undefined) {
            throw Error(`${value.l} have no hash`);
        }
        const hash = value.l.hash;
        const format = yield execAsync(`identify -format "%w,%h," ${`${tempFolder}${hash}.png`}`);
        const [w, h] = format.split(',').map(x => parseInt(x));
        return { hash: hash, x: value.x, y: value.y, width: w, height: h, position_refencial: value.l.position_refencial || undefined };
    }));
    const hashes = yield Promise.all(promiseSave);
    // page them on an empty picture
    // TODO: sortir
    const pageSavedLayers = (value) => {
        const y = value.position_refencial === 'bottom-left' || value.position_refencial === 'bottom-right'
            ? value.y - value.height
            : value.y;
        const x = value.position_refencial === 'top-right' || value.position_refencial === 'bottom-right'
            ? value.x - value.width
            : value.x;
        const tempFile = tempFolder + value.hash + '.png';
        if ((value.width + x) > picture.width) {
            throw Error(`layer w (${value.width} + ${x} = ${value.width + x}) > picture w (${picture.width}) for ` + value.hash);
        }
        if ((value.height + y) > picture.height) {
            throw Error(`layer h (${value.height} + ${y} = ${value.height + y}) > picture h (${picture.height}) for ` + value.hash);
        }
        return `-page +${x}+${y} ${tempFile} +page `;
    };
    hashes.map(value => command += pageSavedLayers(value));
    command += ` -mosaic`;
    if (picture.border) {
        command += ` -shave ${picture.border.size} -bordercolor "${(_a = picture.border.color) !== null && _a !== void 0 ? _a : '#000'}" -border ${picture.border.size}`;
    }
    // TODO : faire un tableau de command au lieu de concat, puis concat à la fin afin d'être sur de pas oublier de foutre des espaces
    // write picture
    return execAsync(command + ` -density 300 -units PixelsPerInch jpg:${path}`);
    // fun test : on peut streamer, mais je sais pas si on peut le faire pour plusieurs images sur un -page ...
    // si on peut, ça veut dire qu'on peut théoriquement se passer de l'écriture disque
    // mais gaffe là en l'état :
    //    diff ./test.png ./tests/huge_picto.png
    //      Les fichiers binaires ./test.png et ./tests/huge_picto.png sont différents
    // const t = await execAsync('convert -background transparent "' + process.env.PWD + '/tests/huge_picto.png" png:- | convert - ./test.png')
    // picture.layers.forEach((layer, z: number) => fs.unlinkSync(tempFolder + '/' + z + '.png'))
    // fs.rmdirSync(tempFolder, {recursive: true})
});
export const makeTextLayer = (config) => {
    return {
        x: config.x,
        y: config.y,
        l: {
            text: config.text,
            font: config.font,
            fontSize: config.fontSize,
            fontColor: config.color,
            center: config.center,
            width: config.width,
            height: config.height,
            weight: config.weight
        }
    };
};
export const makePictureLayer = (x, y, source, config = {}) => {
    assertFileExists(source);
    // config.w / config.h -> valeur en pixel,
    //    ! pour forcer le cassage de ratio, '>' (only shrink larger images. ?
    //    sinon vérifier qu'il n'y ait qu'un des deux et faire '${w}x' ou 'x${h}'
    const resize = config.ratio || 1;
    return {
        x: x,
        y: y,
        l: {
            source: source,
            xResize: resize, // TODO: anamorphic resize
        }
    };
};
export const makeCircleLayer = (x, y, r, color) => {
    return {
        x: x,
        y: y,
        l: {
            shape: 'circle',
            color: color,
            r: r
        }
        // adding 1 pix to image size to be sure
        // command: `convert -size ${2*r + 1}x${2*r + 1} xc:transparent -fill "${color}" -draw "circle ${r},${r} ${2*r},${r}"`
    };
};
// convert -size 10x10 -background transparent -fill "red" -draw "circle 30,30 40,40" /home/jinfante/projects/create-card/tmp/4.png
// convert -size 100x60 xc:skyblue -fill white -stroke black  -draw "circle 50,30 40,10"          draw_circle.gif
// convert -size 100x60 xc:skyblue -fill white -stroke black \
// -draw "circle 50,30 40,10"          draw_circle.gif
export const addBorderToPicture = picture => (border) => {
    if (picture.border)
        throw Error('Picture allready have a border');
    const clone = R.clone(picture);
    clone.border = border;
    return clone;
};
//# sourceMappingURL=image-manipulation.js.map