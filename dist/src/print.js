var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as R from 'ramda';
import { execAsync, identify } from './utils';
import { assertFileExists } from './asserts';
const INCH2CM = 2.54;
const A4_CM_HEIGHT = 29.7;
const A4_CM_WIDTH = 21;
const DPI = 300;
const A4_PIX_HEIGHT = Math.round(A4_CM_HEIGHT / INCH2CM * DPI); // 3508
const A4_PIX_WIDTH = Math.round(A4_CM_WIDTH / INCH2CM * DPI); // 2480
/**
 * Takes a series of jpg files and concat them in A4 jpg files
 * All files should be of the same size and fit in A4
 * It will compute automatically the number of jpeg for each row and column (min 1),
 * and center all of them
 *
 * @param files     individual files to concat
 * @param dest      destination folder
 * @param filename  filename to save, without jpg extension
 * @param spacing   pixel spacing between each jpeg, 0 by default
 */
export const pagine = (files_1, dest_1, filename_1, ...args_1) => __awaiter(void 0, [files_1, dest_1, filename_1, ...args_1], void 0, function* (files, dest, filename, spacing = 0, border = 0) {
    if (filename.length < 1) {
        throw Error(`invalid filename : ${filename}`);
    }
    files.map(assertFileExists);
    const identifies = yield Promise.all(R.map(identify)(files));
    // assert all files are jpegs
    identifies.map(i => { if (i.e !== 'jpg' && i.e !== 'png') {
        throw Error(`${i.f} is not a JPEG or PNG`);
    } });
    // assert all files have the same size
    const jpeg_height = identifies[0].h;
    const jpeg_width = identifies[0].w;
    identifies.map(i => { if (i.w !== jpeg_width) {
        throw Error(`${i.f} does not have the same width (${jpeg_width})`);
    } });
    identifies.map(i => { if (i.h !== jpeg_height) {
        throw Error(`${i.f} does not have the same height (${jpeg_height})`);
    } });
    // TODO: message + clair pour border
    if ((jpeg_height + border) > A4_PIX_HEIGHT && (jpeg_width + border) > A4_PIX_WIDTH) {
        throw Error(`files dimensions (${jpeg_width}x${jpeg_height}) is superior to A4 dimensions (${A4_PIX_WIDTH}x${A4_PIX_HEIGHT})`);
    }
    if ((jpeg_height + 2 * border) > A4_PIX_HEIGHT) {
        throw Error(`files height (${jpeg_height} + ${2 * border}) is superior to A4 height (${A4_PIX_HEIGHT})`);
    }
    if ((jpeg_width + 2 * border) > A4_PIX_WIDTH) {
        throw Error(`files width (${jpeg_width} + ${2 * border}) is superior to A4 width (${A4_PIX_WIDTH})`);
    }
    // x and y repeat computation
    // TODO: function separate, functionnal style ?
    let xRepeat = 1;
    let yRepeat = 1;
    while ((jpeg_width * (xRepeat + 1) + spacing * xRepeat) < (A4_PIX_WIDTH - 2 * border)) {
        xRepeat++;
    }
    while ((jpeg_height * (yRepeat + 1) + spacing * yRepeat) < (A4_PIX_HEIGHT - 2 * border)) {
        yRepeat++;
    }
    const xBorder = Math.floor(((A4_PIX_WIDTH - 2 * border) - (jpeg_width * xRepeat + spacing * (xRepeat - 1))) / 2);
    const yBorder = Math.floor(((A4_PIX_HEIGHT - 2 * border) - (jpeg_height * yRepeat + spacing * (yRepeat - 1))) / 2);
    const pagesCount = Math.ceil(files.length / (xRepeat * yRepeat));
    const digits = pagesCount.toString().length;
    const promises = [...Array(pagesCount)].map((x, page) => __awaiter(void 0, void 0, void 0, function* () {
        let command = `convert "-size" "${A4_PIX_WIDTH}x${A4_PIX_HEIGHT}" "xc:#fff"`;
        files.slice(page * xRepeat * yRepeat, Math.min(files.length, (page + 1) * xRepeat * yRepeat)).map((j, i) => {
            const x = (i % xRepeat) * jpeg_width + spacing * (i % xRepeat) + border;
            const y = Math.floor(i / yRepeat) * jpeg_height + spacing * (Math.floor((i) / yRepeat)) + border;
            command += ` "-page" "${jpeg_width}x${jpeg_height}+${x + xBorder}+${y + yBorder}" ${j}`;
        });
        const pageName = `${dest + filename}${(page + 1).toString().padStart(digits, '0')}.jpg`;
        command += ` "-flatten" "-mosaic"  "jpg:${pageName}"`;
        return execAsync(command)
            .then(() => pageName);
    }));
    return Promise.all(promises);
});
/**
 * Take a series of A4 jpeg files and concat them in one pdf
 * @param jpegs string[]
 * @param filename string
 * @param dest string
 */
export const pdfConcat = (jpegs, filename, dest) => __awaiter(void 0, void 0, void 0, function* () {
    return execAsync(`convert ${jpegs.join(' ')} ${dest + filename}.pdf`)
        .then(() => `${dest + filename}.pdf`);
});
//# sourceMappingURL=print.js.map