var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { exec } from 'child_process';
export const execAsync = command => new Promise((resolve, reject) => {
    exec(command, (error, stdout) => error ? reject(error) : resolve(stdout.trim()));
});
export const identify = (path) => __awaiter(void 0, void 0, void 0, function* () {
    const out = yield execAsync(`identify -format "%f,%e,%h,%w" ${path}`);
    const props = out.split(',');
    return {
        f: props[0],
        e: props[1],
        h: parseInt(props[2]),
        w: parseInt(props[3]),
    };
});
//# sourceMappingURL=utils.js.map