/**
 * Asserts than a file exists
 *
 * @param file
 * @throws Error
 */
export declare const assertFileExists: (file: string) => void;
/**
 * Asserts than a Buffer is a jpeg
 *
 * @param b
 * @throws Error
 */
export declare const assertBufferIsJpeg: (b: Buffer) => void;
