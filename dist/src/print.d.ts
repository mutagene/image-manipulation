/**
 * Takes a series of jpg files and concat them in A4 jpg files
 * All files should be of the same size and fit in A4
 * It will compute automatically the number of jpeg for each row and column (min 1),
 * and center all of them
 *
 * @param files     individual files to concat
 * @param dest      destination folder
 * @param filename  filename to save, without jpg extension
 * @param spacing   pixel spacing between each jpeg, 0 by default
 */
export declare const pagine: (files: string[], dest: string, filename: string, spacing?: number, border?: number) => Promise<string[]>;
/**
 * Take a series of A4 jpeg files and concat them in one pdf
 * @param jpegs string[]
 * @param filename string
 * @param dest string
 */
export declare const pdfConcat: (jpegs: string[], filename: string, dest: string) => Promise<string>;
