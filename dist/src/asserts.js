import * as fs from 'fs';
/**
 * Asserts than a file exists
 *
 * @param file
 * @throws Error
 */
export const assertFileExists = file => {
    if (!fs.existsSync(file)) {
        throw Error(`${file} does not exists`);
    }
};
/**
 * Asserts than a Buffer is a jpeg
 *
 * @param b
 * @throws Error
 */
export const assertBufferIsJpeg = (b) => {
    if (!b || b.length <= 2 || b[0] !== 255 || b[1] !== 216 || b[2] !== 255) {
        throw Error(`${Buffer} is not a JPEG Buffer`);
    }
};
//# sourceMappingURL=asserts.js.map