export declare const execAsync: (command: string) => Promise<string>;
export type IdentifyResult = {
    f: string;
    e: string;
    h: number;
    w: number;
};
export declare const identify: (path: string) => Promise<IdentifyResult>;
