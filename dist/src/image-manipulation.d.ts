export declare const WIDTH = 635;
export declare const HEIGHT = 880;
/**
 * Common part for the different layers types, mostly for basic transformations
 */
interface CommonLayer {
    width?: number;
    height?: number;
    x?: number;
    y?: number;
    v_align?: number;
    h_align?: number;
    xResize?: number;
    yResize?: number;
    rotate?: number;
    position_refencial?: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right';
    hash?: string;
}
export interface ImageLayer extends CommonLayer {
    source: string;
}
export type weight = 'ultralight' | 'light' | 'normal' | 'bold' | 'ultrabold' | 'heavy';
export interface TextLayer extends CommonLayer {
    text: string;
    font: string;
    fontSize: number;
    fontColor?: string;
    weight?: weight;
    center?: boolean;
}
export interface DrawingLayer extends CommonLayer {
    shape: 'circle';
    color: string;
    r?: number;
}
export type Layer = ImageLayer | TextLayer | DrawingLayer;
export type PositionedLayer = {
    x: number;
    y: number;
    l: Layer | LayerPile;
};
export interface LayerPile extends CommonLayer {
    align: 'top' | 'right' | 'bottom' | 'left';
    layers: Array<Layer | LayerPile>;
}
export interface Border {
    size: number;
    color?: string;
}
export interface Picture {
    width: number;
    height: number;
    layers: Array<PositionedLayer>;
    border?: Border;
}
/**
 * Calcul du hash d'un Layer ou LayerPile, unique, contenant toutes les infos du layer
 * On ne calcule plus le hash des images sources comme avant,
 *    donc 2 sources identiques mais avec url différent donnent 2 hash !==
 */
export declare const getHash: (layer: Layer | LayerPile) => string;
/**
 * parcours les Layer et construit un tableau de profondeur avec les layer
 * en supprimant les doublons (et en mettant dans la profondeur la plus élevée le cas échéant)
 */
export declare const completeData: (data: (Layer | LayerPile)[][], layer: Layer | LayerPile, deep?: number) => (Layer | LayerPile)[][];
export declare const addLayerToData: (data: (Layer | LayerPile)[][], layer: Layer | LayerPile, deep?: number) => (Layer | LayerPile)[][];
export declare const makeIt: (l: (Layer | LayerPile)[]) => Promise<string[]>;
export declare const computeLayerHash: (l: Layer | LayerPile) => Layer | LayerPile;
export declare const compilePicture: (pic: Picture, path: string) => Promise<string>;
export declare const makeTextLayer: (config: {
    x: number;
    y: number;
    fontSize: number;
    font: string;
    text: string;
    width?: number;
    height?: number;
    weight?: weight;
    center?: boolean;
    color?: string;
}) => {
    x: number;
    y: number;
    l: TextLayer;
};
export declare const makePictureLayer: (x: number, y: number, source: string, config?: {
    ratio?: number;
}) => PositionedLayer;
export declare const makeCircleLayer: (x: number, y: number, r: number, color: string) => {
    x: number;
    y: number;
    l: DrawingLayer;
};
export declare const addBorderToPicture: (picture: Picture) => (border: Border) => Picture;
export {};
