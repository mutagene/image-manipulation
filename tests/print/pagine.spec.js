var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { assert, expect } from 'chai';
import * as fs from 'fs';
import * as path from 'path';
import * as tmp from 'tmp';
import { identify } from '../../src/utils';
import { pagine } from '../../src/print';
const LENA_JPG = process.env.PWD + '/tests/fixtures/lena.jpg';
const LENA_JPG_SMALL = process.env.PWD + '/tests/fixtures/lenn_small.jpg';
const LENA_JPG_BIG = process.env.PWD + '/tests/fixtures/lena_big.jpg';
const LENA_PNG = process.env.PWD + '/tests/fixtures/lena.png';
describe('files validity for pagine', () => {
    let tmpobj, tempFolder;
    // make a temp dir
    beforeEach(() => {
        tmpobj = tmp.dirSync();
        tempFolder = tmpobj.name + path.sep;
    });
    // empty and remove the temp dir
    afterEach(() => {
        fs.readdirSync(tempFolder).map(f => fs.unlinkSync(tempFolder + f));
        tmpobj.removeCallback();
    });
    it('fails on not jpeg picture', () => pagine([LENA_JPG, LENA_PNG], tempFolder, 'test')
        .then(() => Promise.reject(new Error('Expected method to reject.')), err => assert.instanceOf(err, Error)));
    it('fails on not pictures with different sizes', () => pagine([LENA_JPG, LENA_JPG_SMALL], tempFolder, 'test')
        .then(() => Promise.reject(new Error('Expected method to reject.')), err => assert.instanceOf(err, Error)));
    it('fails on too big picture', () => pagine([LENA_JPG_BIG], tempFolder, 'test')
        .then(() => Promise.reject(new Error('Expected method to reject.')), err => {
        assert.instanceOf(err, Error);
        assert.include(err.message, 'is superior to A4 dimensions');
    }));
    // TODO: tests borders
});
describe('Working', () => __awaiter(void 0, void 0, void 0, function* () {
    let tmpobj, tempFolder;
    // make a temp dir
    beforeEach(() => {
        tmpobj = tmp.dirSync();
        tempFolder = tmpobj.name + path.sep;
    });
    // empty and remove the temp dir
    afterEach(() => {
        fs.readdirSync(tempFolder).map(f => fs.unlinkSync(tempFolder + f));
        tmpobj.removeCallback();
    });
    it('1 picture make 1 page', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const files = Array(1).fill(LENA_JPG);
            const results = yield pagine(files, tempFolder, 'test', 100);
            expect(results.length).eql(1);
            this.timeout(10000);
            // todo vérifier que le fichier existe
            const idResult = yield Promise.all(results.map(identify));
            idResult.map(r => {
                expect(r.e).eq('jpg');
                expect(r.h).eq(3508);
                expect(r.w).eq(2480);
            });
        });
    });
    it('10 picture make 2 pages', function () {
        return __awaiter(this, void 0, void 0, function* () {
            const files = Array(10).fill(LENA_JPG);
            const results = yield pagine(files, tempFolder, 'test', 100);
            expect(results.length).eql(2);
            this.timeout(10000);
            // todo vérifier que le fichier existe
            const idResult = yield Promise.all(results.map(identify));
            idResult.map(r => {
                expect(r.e).eq('jpg');
                expect(r.h).eq(3508);
                expect(r.w).eq(2480);
            });
        });
    });
}));
