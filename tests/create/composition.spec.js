import { assert } from 'chai';
import * as fs from 'fs';
import * as path from 'path';
import * as tmp from 'tmp';
import { compilePicture } from '../../src/image-manipulation';
describe('Resize with ratio', () => {
    let tmpobj, tempFolder;
    const HUGE_PICTO = process.env.PWD + '/tests/huge_picto.png';
    // make a temp dir
    beforeEach(() => {
        tmpobj = tmp.dirSync();
        tempFolder = tmpobj.name + path.sep;
    });
    // empty and remove the temp dir
    afterEach(() => {
        fs.readdirSync(tempFolder).map(f => fs.unlinkSync(tempFolder + f));
        tmpobj.removeCallback();
    });
    it('fails on not too big picture after resize', () => {
        const pictureLayer = {
            source: HUGE_PICTO,
            xResize: 1,
            yResize: 1
        };
        const testPicture = {
            width: 1500,
            height: 1500,
            layers: [
                {
                    x: 120,
                    y: 120,
                    l: pictureLayer,
                }
            ]
        };
        return compilePicture(testPicture, tempFolder + 'export.jpg')
            .then(() => Promise.reject(new Error('Expected method to reject.')), err => assert.instanceOf(err, Error));
    });
});
