import * as fs from 'fs'
import * as path from 'path'
import * as tmp from 'tmp'
import { ImageLayer, Picture, compilePicture } from '../../src/image-manipulation'

describe('CompilePicture', () => {
  let tmpobj: tmp.DirResult,
    tempFolder: string

  const HUGE_PICTO = process.env.PWD + '/tests/huge_picto.png'

  // make a temp dir
  beforeEach(() => {
    tmpobj = tmp.dirSync()
    tempFolder = tmpobj.name + path.sep
  })

  // empty and remove the temp dir
  afterEach(() => {
    fs.readdirSync(tempFolder).map((f: string) => fs.unlinkSync(tempFolder + f))
    tmpobj.removeCallback()
  })

  it('works', () => {
    const pictureLayer: ImageLayer = {
      source: HUGE_PICTO,
      xResize: .1,
      yResize: .1
    }
    const testPicture: Picture = {
      width: 1500,
      height: 1500,
      layers: [
        {
          x: 120,
          y: 120,
          l: pictureLayer,
        }
      ]
    }
    return compilePicture(testPicture, tempFolder + 'export.jpg')
  })

  it('create ony one temp layer for 2 imagesLayer with same source and different resize', async () => {

    // !!!!! ici tester ce qu'il faut sortir (process data)

    const pictureLayer1: ImageLayer = {
      source: HUGE_PICTO,
      xResize: .1,
    }
    const pictureLayer2: ImageLayer = {
      source: HUGE_PICTO,
      xResize: .2,
    }
    const testPicture: Picture = {
      width: 1500,
      height: 1500,
      layers: [
        {
          x: 0,
          y: 0,
          l: pictureLayer1,
        },
        {
          x: 120,
          y: 120,
          l: pictureLayer2,
        }
      ]
    }
    await compilePicture(testPicture, tempFolder + 'export.jpg')
  })
})
