var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as fs from 'fs';
import * as path from 'path';
import * as tmp from 'tmp';
import { compilePicture } from '../../src/image-manipulation';
describe('CompilePicture', () => {
    let tmpobj, tempFolder;
    const HUGE_PICTO = process.env.PWD + '/tests/huge_picto.png';
    // make a temp dir
    beforeEach(() => {
        tmpobj = tmp.dirSync();
        tempFolder = tmpobj.name + path.sep;
    });
    // empty and remove the temp dir
    afterEach(() => {
        fs.readdirSync(tempFolder).map((f) => fs.unlinkSync(tempFolder + f));
        tmpobj.removeCallback();
    });
    it('works', () => {
        const pictureLayer = {
            source: HUGE_PICTO,
            xResize: .1,
            yResize: .1
        };
        const testPicture = {
            width: 1500,
            height: 1500,
            layers: [
                {
                    x: 120,
                    y: 120,
                    l: pictureLayer,
                }
            ]
        };
        return compilePicture(testPicture, tempFolder + 'export.jpg');
    });
    it('create ony one temp layer for 2 imagesLayer with same source and different resize', () => __awaiter(void 0, void 0, void 0, function* () {
        // !!!!! ici tester ce qu'il faut sortir (process data)
        const pictureLayer1 = {
            source: HUGE_PICTO,
            xResize: .1,
        };
        const pictureLayer2 = {
            source: HUGE_PICTO,
            xResize: .2,
        };
        const testPicture = {
            width: 1500,
            height: 1500,
            layers: [
                {
                    x: 0,
                    y: 0,
                    l: pictureLayer1,
                },
                {
                    x: 120,
                    y: 120,
                    l: pictureLayer2,
                }
            ]
        };
        yield compilePicture(testPicture, tempFolder + 'export.jpg');
    }));
});
