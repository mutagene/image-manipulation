import { expect } from 'chai';
import * as fs from 'fs';
import * as path from 'path';
import * as tmp from 'tmp';
import { getHash, computeLayerHash, completeData } from '../../src/image-manipulation';
describe('getHash', () => {
    let tmpobj, tempFolder;
    // make a temp dir
    beforeEach(() => {
        tmpobj = tmp.dirSync();
        tempFolder = tmpobj.name + path.sep;
    });
    // empty and remove the temp dir
    afterEach(() => {
        fs.readdirSync(tempFolder).map(f => fs.unlinkSync(tempFolder + f));
        tmpobj.removeCallback();
    });
    it('is different on 2 ImageLayers with 2 identical files with different url', () => {
        const file1 = `${tempFolder}file1.jpg`;
        const file2 = `${tempFolder}file2.jpg`;
        const file3 = `${tempFolder}file3.jpg`;
        fs.writeFileSync(file1, 'foo');
        fs.writeFileSync(file2, 'foo');
        fs.writeFileSync(file3, 'bar');
        const layer1 = { source: file1 };
        const layer2 = { source: file2 };
        const layer3 = { source: file3 };
        expect(getHash(layer1)).is.not.eq(getHash(layer2));
        expect(getHash(layer1)).is.not.eq(getHash(layer3));
    });
    it('works on LayerPile', () => {
        const file1 = `${tempFolder}file1.jpg`;
        const file2 = `${tempFolder}file2.jpg`;
        fs.writeFileSync(file1, 'foo');
        fs.writeFileSync(file2, 'foo');
        const pile1 = {
            align: 'bottom',
            layers: [
                { source: file1, xResize: .1 },
            ]
        };
        const pile2 = {
            align: 'bottom',
            layers: [
                { source: file2, xResize: .1 },
            ]
        };
        const pile3 = {
            align: 'bottom',
            layers: [
                { source: file2, xResize: .2 },
            ]
        };
        expect(getHash(pile1)).is.not.eq(getHash(pile2)); // different sources
        expect(getHash(pile2)).is.not.eq(getHash(pile3)); // same source, different ratio
    });
});
describe('computeLayerHash', () => {
    let tmpobj, tempFolder;
    // make a temp dir
    beforeEach(() => {
        tmpobj = tmp.dirSync();
        tempFolder = tmpobj.name + path.sep;
    });
    // empty and remove the temp dir
    afterEach(() => {
        fs.readdirSync(tempFolder).map(f => fs.unlinkSync(tempFolder + f));
        tmpobj.removeCallback();
    });
    it('works on a Layer', () => {
        const file = `${tempFolder}file1.jpg`;
        fs.writeFileSync(file, 'foo');
        const layer = { source: file };
        const layerWithHash = computeLayerHash(layer);
        expect(getHash(layer)).is.eq(layerWithHash.hash);
    });
    it('computeLayerHash on LayerPile', () => {
        const file1 = `${tempFolder}file1.jpg`;
        fs.writeFileSync(file1, 'foo');
        const pile1 = {
            align: 'bottom',
            layers: [
                { source: file1, xResize: .1 },
            ]
        };
        expect(getHash(pile1)).is.eq(computeLayerHash(pile1).hash);
    });
    // TODO: sortir
    it('completeData does not create doublon', () => {
        console.log(tempFolder);
        const file1 = `${tempFolder}file1.jpg`;
        const file2 = `${tempFolder}file2.jpg`;
        fs.writeFileSync(file1, 'foo');
        fs.writeFileSync(file2, 'bar');
        const layers = [
            {
                align: 'bottom',
                layers: [
                    { source: file1, xResize: .1 },
                    { source: file1 },
                    { source: file2 },
                    {
                        align: 'top',
                        layers: [
                            { source: file1 },
                            { source: file2 }
                        ]
                    },
                ]
            },
            { source: file1 }
        ];
        let data = [];
        for (const layer of layers) {
            data = completeData(data, computeLayerHash(layer));
        }
        data = data.filter(x => x !== null);
        expect(data.length).eq(3);
        const layers2 = [
            {
                align: 'bottom',
                layers: [
                    { source: file1, xResize: .1 },
                    { source: file1 },
                    { source: file2 },
                    {
                        align: 'top',
                        layers: [
                            { source: file1 },
                            { source: file2 },
                            {
                                align: 'bottom',
                                layers: [
                                    { source: file1, xResize: .1 },
                                    { source: file1, xResize: .2 },
                                    { source: file2, xResize: .1 },
                                ]
                            },
                        ]
                    },
                ]
            },
            { source: file1 }
        ];
        let data2 = [];
        for (const layer of layers2) {
            data2 = completeData(data2, computeLayerHash(layer));
        }
        data2 = data2.filter(x => x !== null);
        expect(data2.length).eq(4);
        const layers3 = [
            { source: file1 },
            { source: file2 }
        ];
        let data3 = [];
        for (const layer of layers3) {
            data3 = completeData(data3, computeLayerHash(layer));
        }
        data3 = data3.filter(x => x !== null);
        expect(data3.length).eq(1);
    });
});
