// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-undef, @typescript-eslint/explicit-function-return-type
module.exports = (wallaby) => ({
  files: [
    'src/*.ts',
    'config.ts',
    'tsconfig.json',
    '!src/**/*.spec.ts',
  ],

  filesWithNoCoverageCalculated: [
    'src/*/*.interfaces.ts',
  ],

  tests: [
    'tests/**/*.spec.ts'
  ],

  env: {
    type: 'node',
    runner: 'node',
  },

  testFramework: 'mocha',


  workers: {  // à voir si nécessaire
    initial: 6,
    regular: 4,
    restart: false,
  }
  // TODO: ok pour wallaby, mais mocha lancé via npm ET ide : nok
  // setup: wallaby => {
  //   global.expect = require('chai').expect;
  // },

})