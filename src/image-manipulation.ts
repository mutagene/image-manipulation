import * as fs from 'fs'
import * as R from 'ramda'
import md5 from 'js-md5'
import { execAsync } from './utils'
import { assertFileExists } from './asserts'

export const WIDTH = 635
export const HEIGHT = 880

const tempFolder: string = (process.env.TMP || process.env.PWD + '/tmp') + '/'


/**
 * Common part for the different layers types, mostly for basic transformations
 */
interface CommonLayer {
  width?: number // if undefined: automatic calculation
  height?: number // if undefined: automatic calculation
  x?: number // vertical translation, 0 by default
  y?: number // horizontal translation, 0 by default
  v_align?: number // can't be associated with a x
  h_align?: number // can't be associated with a y
  xResize?: number
  yResize?: number
  rotate?: number
  // TODO: changer le nom, en gros c'est l'angle à partir duquel on veut positionner l'image
  // à voir si utile ...
  position_refencial?: 'top-left'|'top-right'|'bottom-left'|'bottom-right' // 'top-left' by default
  hash?: string // si le hash a été calculé
}

export interface ImageLayer extends CommonLayer {
  source: string
}

const isImageLayer = (layer: Layer): layer is ImageLayer =>
  (layer as ImageLayer).source !== undefined

export type weight = 'ultralight'|'light'|'normal'|'bold'|'ultrabold'|'heavy'

export interface TextLayer extends CommonLayer {
  text: string
  font: string
  fontSize: number
  fontColor?: string
  weight?: weight
  center?: boolean
}

const isTextLayer = (layer: Layer): layer is TextLayer =>
  (layer as TextLayer).text !== undefined


export interface DrawingLayer extends CommonLayer {
  shape: 'circle'
  color: string
  r?: number
}

const isDrawingLayer = (layer: Layer): layer is DrawingLayer =>
  (layer as DrawingLayer).shape !== undefined

export type Layer = ImageLayer | TextLayer | DrawingLayer

export type PositionedLayer = {
  x: number,
  y: number,
  l: Layer | LayerPile
}

const isLayer = (layer: Layer | LayerPile): layer is Layer =>
  isImageLayer(layer as Layer) || isTextLayer(layer as Layer) || isDrawingLayer(layer as Layer)
  && (layer as LayerPile).align === undefined


export interface LayerPile extends CommonLayer {
  align: 'top'|'right'|'bottom'|'left'
  layers: Array<Layer|LayerPile>,
}

const isLayerPile = (layer: Layer | LayerPile): layer is LayerPile =>
  !(isImageLayer(layer as Layer) || isTextLayer(layer as Layer) || isDrawingLayer(layer as Layer))
  && (layer as LayerPile).align !== undefined

export interface Border {
  size: number,
  color?: string // black by default
}

export interface Picture {
  width: number,
  height: number,
  layers: Array<PositionedLayer>, // TODO: utiliser une meilleur structure de donnée ? besoin de permettre 2 layers avec les même coord
  border?: Border
}

/**
 * Extrait la commande du layer
 */
const getCommand: (layer: Layer) => string = (layer) => {
  if (isImageLayer(layer)) {
    assertFileExists(layer.source)
    // config.w / config.h -> valeur en pixel,
    //    ! pour forcer le cassage de ratio, '>' (only shrink larger images. ?
    //    sinon vérifier qu'il n'y ait qu'un des deux et faire '${w}x' ou 'x${h}'

    const resize: number = 100 * (layer.xResize || 1) // TODO: gérer le resize via ratio propre, ?
    return `convert -background transparent -resize ${resize}% ${layer.source}`
  }
  if (isTextLayer(layer)) {
    const width = layer.width || 0
    const height = layer.height || 0
    const center = layer.center || false
    const color = layer.fontColor || 'black'
    const weight = layer.weight || 'normal'
    // si on spécifie une size, on est en text align justify automatiquement
    const size = `-size ${width === 0 ? '' : width}x${height === 0 ? '' : height}`
    const align = center ? '-gravity center' : ''
    return `convert -background transparent ${size} ${align} -fill ${color} "pango:<span size=\\"${layer.fontSize * 1000}\\" font_desc=\\"${layer.font}\\" weight=\\"${weight}\\">${layer.text}</span>"`
  }
  if (isDrawingLayer(layer)) {

    if (layer.shape === 'circle') {
      if (layer.r === undefined) {
        throw new Error('circle with no r value')
      }
      return `convert -size ${2*layer.r + 1}x${2*layer.r + 1} xc:transparent -fill "${layer.color}" -draw "circle ${layer.r},${layer.r} ${2*layer.r},${layer.r}"`
    }

    throw new Error(`Unhandled shape ${layer.shape} for DrawingLayer`)

  }
  throw new Error('layer is not a Layer')
}


/**
 * Calcul du hash d'un Layer ou LayerPile, unique, contenant toutes les infos du layer
 * On ne calcule plus le hash des images sources comme avant,
 *    donc 2 sources identiques mais avec url différent donnent 2 hash !==
 */
export const getHash: (layer: Layer | LayerPile) => string = layer => {
  if (layer.hash !== undefined) return layer.hash

  // TODO: issue #8
  // layer.xResize = undefined
  // layer.yResize = undefined
  // layer.rotate = undefined

  return isLayerPile(layer)
    ? md5(JSON.stringify({...layer, layers: layer.layers.map(sub => getHash(sub))}))
    : md5(JSON.stringify(layer))
}

/**
 * True si le fichier ${hash}.png existe dans le dossier temporaire
 */
const fileHashExists: (hash:string) => boolean = hash => fs.existsSync(`${tempFolder}/${hash}.png`)

/**
 * Sauvegarde le layer (ou ne fait rien s'il existe déjà), retourne le hash
 */
const saveLayer: (layer: Layer | LayerPile) => Promise<string> = async (layer) => {
  const hash: string = getHash(layer)

  // existe déjà -> rien à faire
  // TODO: pour imageLayer, on va regarder si l image existe, pas de hash.jpg
  if (fileHashExists(hash)) {
    return hash
  }

  // pour un layer simple, on appelle sa commande
  if (isLayer(layer)) {
    await execAsync(`${getCommand(layer)} ${tempFolder}/${hash}.png`)

    return hash
  }

  // LayerPile, plus de taf
  if (isLayerPile(layer)) {
    // TODO: enregistrer une image intermédiaire qui est composée de ces layers, en calculant le stack s'il y a lieu
    // pour chaque layer on lance saveLayer de la même manière

    // TODO: functionnal
    const savedLayers: SavedLayer[] = []
    // TODO: pour l'instant marche juste en bottom
    const x = 0
    let y = 0

    for (const current of layer.layers) {

      const subhash: string = await saveLayer(current) // peut ne rien faire s'il existe déjà

      // calcul de la taille de l'image obtenue
      const format: string = await execAsync(`identify -format "%w,%h," ${`${tempFolder}${subhash}.png`}`)
      const [w, h] = format.split(',')

      savedLayers.push({hash: subhash, x: x, y: y, width: parseInt(w), height: parseInt(h)})

      // TODO: position_refencial doit être défini dans un pile layer bottom par exemple ? ou à la main pour le moment ?

      // switch (layer.align) {
      //   case 'bottom':

      //     break;

      //   default:
      //     break;
      // }
      // x += parseInt(w) // TODO: calcul selon le stack du layer et la taille du précédent l saved
      y += parseInt(h)
    }

    let command = `convert -background transparent `
    // fs.mkdirSync(tempFolder)

    const hashes: SavedLayer[] = await Promise.all(savedLayers)
    // page them on an empty picture

    // TODO: sortir
    const pageSavedLayers: (value: SavedLayer) => string = (value) => {
      return `-page +${value.x}+${value.y} ${tempFolder + value.hash + '.png'} +page `
    }

    hashes.map(value => command += pageSavedLayers(value))

    // TODO: faire un tableau de command au lieu de concat, puis concat à la fin afin d'être sur de pas oublier de foutre des espaces

    // write picture
    await execAsync(command + ` -mosaic png:${tempFolder + hash + '.png'}`)

    // page le tout avec le stack en utilisant les tailles

    return hash
  }
  // TODO:

  throw new Error('this is not a Layer or LayerPile')
}

interface SavedLayer {
  hash: string
  x: number
  y: number
  width: number
  height: number
  position_refencial?: string
}

// const OVERDRIVE = false // TODO: parametric

/**
 * parcours les Layer et construit un tableau de profondeur avec les layer
 * en supprimant les doublons (et en mettant dans la profondeur la plus élevée le cas échéant)
 */
export const completeData: (data: (Layer | LayerPile)[][], layer: Layer | LayerPile, deep?: number) => (Layer | LayerPile)[][] = (data, layer, deep = 0) => {
  if (!isLayerPile(layer)) {
    return addLayerToData(data, layer, deep)
  }

  data = addLayerToData(data, layer, deep)
  for (const l of layer.layers) {
    data = completeData(data, l, deep + 1)
  }

  return data
}

export const addLayerToData: (data: (Layer | LayerPile)[][], layer: Layer | LayerPile, deep?: number) => (Layer | LayerPile)[][] = (data, layer, deep = 0) => {
  if (layer.hash === undefined) {
    throw Error(`${layer} have no hash`)
  }

  // on regarde si le layer est déjà dans les data
  for (const [d, v] of (data || []).entries()) {
    for (const [key] of (v || []).entries()) {
      if (v[key].hash === layer.hash) {
        // si oui, on compare la deep
        if (deep > d) {
          // plus profond ce que qui est déjà dans data :
          // on ajoute en supprimant l'ancienne

          data[d].splice(key, key)
          data[deep] = data[deep] ?? []
          data[deep].push(layer)
        }
        return data
      }
    }
  }
  // si non on l'ajoute
  data[deep] = data[deep] ?? []
  data[deep].push(layer)

  return data
}

// ça dégage: il suffit de map
// export const compilePictures: (h: number, w: number) => (pics: PictureToCompile[]) => Promise<string[]> = (h, w) => {return async pics => {
//   // calcul du hash de chaque Layer et LayerPile (données modifiées direct)
//   for (const picKey in pics) {
//     for (const layKey in pics[picKey].picture.layers) {
//       pics[picKey].picture.layers[layKey].l = computeLayerHash(pics[picKey].picture.layers[layKey].l)
//     }
//   }
//   // TODO: sortir ça propre et tester cf !!!!!
//   let dataToProcess: (Layer | LayerPile)[][] = []
//   for (const picKey in pics) {
//     for (const layer of pics[picKey].picture.layers) {
//       dataToProcess = completeData(dataToProcess, layer.l)
//     }
//   }
//   dataToProcess = dataToProcess.filter(x => x !== null && x !== [])
//   // on a tous les calques avec leur hash, rangés par unicité de hash dans un tableau de deep
//   // pour chacun, soit existe, soit on doit la faire
//   for (const key in dataToProcess) {
//     await makeIt(dataToProcess[key])
//   }
//   return Promise.all(pics.map(compilePicture(h, w)))
// }}

export const makeIt: (l: (Layer | LayerPile)[]) => Promise<string[]> = async (arr) => {
  return Promise.all(arr.map(saveLayer))
}

// calcule le hash d'un layer et lui ajoute, de manière récursive si LayerPile
export const computeLayerHash: (l: Layer | LayerPile) => Layer | LayerPile = (l) => {

  if (isLayerPile(l)) {
    const hashedLayers = l.layers.map(computeLayerHash)
    return {...l, layers: hashedLayers, hash: getHash({...l, layers: hashedLayers})}
  }

  return {...l, hash: getHash(l)}
}

export const compilePicture: (pic: Picture, path: string) => Promise<string> = async (picture, path) => {
  const h = picture.height
  const w = picture.width
  let command = `convert -size ${w}x${h} xc:#fff `

  for (const layKey in picture.layers) {
    picture.layers[layKey].l = computeLayerHash(picture.layers[layKey].l)
  }

  let dataToProcess: (Layer | LayerPile)[][] = []
  for (const layer of picture.layers) {
    dataToProcess = completeData(dataToProcess, layer.l)
  }

  dataToProcess = dataToProcess.filter(x => x !== null)
  // on a tous les calques avec leur hash, rangés par unicité de hash dans un tableau de deep
  // pour chacun, soit existe, soit on doit la faire
  for (const key in dataToProcess) {
    await makeIt(dataToProcess[key])
  }

  // write all layer in temp with name = hash
  const promiseSave: Promise<SavedLayer>[] = picture.layers.map(
    async (value: {x: number, y: number, l: Layer | LayerPile}) => {
      if (value.l.hash === undefined) {
        throw Error(`${value.l} have no hash`)
      }
      const hash: string = value.l.hash
      const format: string = await execAsync(`identify -format "%w,%h," ${`${tempFolder}${hash}.png`}`)
      const [w, h] = format.split(',').map(x => parseInt(x))
      return {hash: hash, x: value.x, y: value.y, width: w, height: h, position_refencial: value.l.position_refencial || undefined}
    }
  )
  const hashes: SavedLayer[] = await Promise.all(promiseSave)
  // page them on an empty picture

  // TODO: sortir
  const pageSavedLayers: (value: SavedLayer) => string = (value) => {

    const y: number = value.position_refencial === 'bottom-left' || value.position_refencial === 'bottom-right'
      ? value.y - value.height
      : value.y

    const x: number = value.position_refencial === 'top-right' || value.position_refencial === 'bottom-right'
      ? value.x - value.width
      : value.x

    const tempFile: string = tempFolder + value.hash + '.png'
    if ((value.width + x) > picture.width) {
      throw Error(`layer w (${value.width} + ${x} = ${value.width + x}) > picture w (${picture.width}) for ` + value.hash)
    }
    if ((value.height + y) > picture.height) {
      throw Error(`layer h (${value.height} + ${y} = ${value.height + y}) > picture h (${picture.height}) for ` + value.hash)
    }

    return `-page +${x}+${y} ${tempFile} +page `
  }

  hashes.map(value => command += pageSavedLayers(value))

  command += ` -mosaic`

  if (picture.border) {
    command += ` -shave ${picture.border.size} -bordercolor "${picture.border.color ?? '#000'}" -border ${picture.border.size}`
  }
  // TODO : faire un tableau de command au lieu de concat, puis concat à la fin afin d'être sur de pas oublier de foutre des espaces

  // write picture
  return execAsync(command + ` -density 300 -units PixelsPerInch jpg:${path}`)

  // fun test : on peut streamer, mais je sais pas si on peut le faire pour plusieurs images sur un -page ...
  // si on peut, ça veut dire qu'on peut théoriquement se passer de l'écriture disque
  // mais gaffe là en l'état :
  //    diff ./test.png ./tests/huge_picto.png
  //      Les fichiers binaires ./test.png et ./tests/huge_picto.png sont différents
  // const t = await execAsync('convert -background transparent "' + process.env.PWD + '/tests/huge_picto.png" png:- | convert - ./test.png')

  // picture.layers.forEach((layer, z: number) => fs.unlinkSync(tempFolder + '/' + z + '.png'))
  // fs.rmdirSync(tempFolder, {recursive: true})
}

export const makeTextLayer: (config: {x: number, y: number, fontSize: number, font: string, text: string, width?: number, height?: number, weight?: weight,center?: boolean, color?: string}) => {x: number, y: number, l: TextLayer} =
  (config) => {
    return {
      x: config.x,
      y: config.y,
      l: {
        text: config.text,
        font: config.font,
        fontSize: config.fontSize,
        fontColor: config.color,
        center: config.center,
        width: config.width,
        height: config.height,
        weight: config.weight
      }
    }
  }

export const makePictureLayer: (x: number, y: number, source: string, config?: {ratio?: number}) => PositionedLayer =
  (x, y, source, config = {}) => {
    assertFileExists(source)
    // config.w / config.h -> valeur en pixel,
    //    ! pour forcer le cassage de ratio, '>' (only shrink larger images. ?
    //    sinon vérifier qu'il n'y ait qu'un des deux et faire '${w}x' ou 'x${h}'

    const resize: number = config.ratio || 1

    return {
      x: x,
      y: y,
      l: {
        source: source,
        xResize: resize, // TODO: anamorphic resize
      }
    }
  }

export const makeCircleLayer: (x: number, y: number, r: number, color: string) => {x: number, y: number, l: DrawingLayer} =
  (x, y, r, color) => {
    return {
      x: x,
      y: y,
      l: {
        shape: 'circle',
        color: color,
        r: r
      }
      // adding 1 pix to image size to be sure
      // command: `convert -size ${2*r + 1}x${2*r + 1} xc:transparent -fill "${color}" -draw "circle ${r},${r} ${2*r},${r}"`
    }
  }
// convert -size 10x10 -background transparent -fill "red" -draw "circle 30,30 40,40" /home/jinfante/projects/create-card/tmp/4.png
// convert -size 100x60 xc:skyblue -fill white -stroke black  -draw "circle 50,30 40,10"          draw_circle.gif
// convert -size 100x60 xc:skyblue -fill white -stroke black \
// -draw "circle 50,30 40,10"          draw_circle.gif

export const addBorderToPicture: (picture: Picture) => (border: Border) => Picture =
  picture => (border: Border): Picture => {
    if (picture.border) throw Error('Picture allready have a border')
    const clone: Picture = R.clone(picture)
    clone.border = border
    return clone
  }
