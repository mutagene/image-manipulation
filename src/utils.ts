
import { exec, ExecException } from 'child_process'



export const execAsync: (command: string) => Promise<string> = command => new Promise((resolve, reject): void => {
  exec(command, (error: ExecException | null, stdout: string) => error ? reject(error) : resolve(stdout.trim()))
})

export type IdentifyResult = {f: string, e: string, h: number, w: number}

export const identify: (path: string) => Promise<IdentifyResult> = async (path) => {
  const out: string = await execAsync(`identify -format "%f,%e,%h,%w" ${path}`)
  const props: string[] = out.split(',')
  return {
    f: props[0],
    e: props[1],
    h: parseInt(props[2]),
    w: parseInt(props[3]),
  }
}