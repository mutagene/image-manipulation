export {
    compilePicture,
    makePictureLayer,
    makeTextLayer,
    makeCircleLayer,
    addBorderToPicture,
    Picture,
    Layer,
    ImageLayer,
    TextLayer,
    DrawingLayer,
    PositionedLayer,
  } from './src/image-manipulation'
   
  export {
    pagine,
    pdfConcat,
  } from './src/print'
   
  export {
    assertFileExists
  } from './src/asserts'